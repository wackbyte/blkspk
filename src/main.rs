use std::env;

fn translate<'a>(c: char) -> Option<&'a str> {
    match c.to_ascii_lowercase() {
        'a' => Some("\u{1F1E6}"),
        'b' => Some("\u{1F1E7}"),
        'c' => Some("\u{1F1E8}"),
        'd' => Some("\u{1F1E9}"),
        'e' => Some("\u{1F1EA}"),
        'f' => Some("\u{1F1EB}"),
        'g' => Some("\u{1F1EC}"),
        'h' => Some("\u{1F1ED}"),
        'i' => Some("\u{1F1EE}"),
        'j' => Some("\u{1F1EF}"),
        'k' => Some("\u{1F1F0}"),
        'l' => Some("\u{1F1F1}"),
        'm' => Some("\u{1F1F2}"),
        'n' => Some("\u{1F1F3}"),
        'o' => Some("\u{1F1F4}"),
        'p' => Some("\u{1F1F5}"),
        'q' => Some("\u{1F1F6}"),
        'r' => Some("\u{1F1F7}"),
        's' => Some("\u{1F1F8}"),
        't' => Some("\u{1F1F9}"),
        'u' => Some("\u{1F1FA}"),
        'v' => Some("\u{1F1FB}"),
        'w' => Some("\u{1F1FC}"),
        'x' => Some("\u{1F1FD}"),
        'y' => Some("\u{1F1FE}"),
        'z' => Some("\u{1F1FF}"),
        '0' => Some("0\u{FE0F}\u{20E3}"),
        '1' => Some("1\u{FE0F}\u{20E3}"),
        '2' => Some("2\u{FE0F}\u{20E3}"),
        '3' => Some("3\u{FE0F}\u{20E3}"),
        '4' => Some("4\u{FE0F}\u{20E3}"),
        '5' => Some("5\u{FE0F}\u{20E3}"),
        '6' => Some("6\u{FE0F}\u{20E3}"),
        '7' => Some("7\u{FE0F}\u{20E3}"),
        '8' => Some("8\u{FE0F}\u{20E3}"),
        '9' => Some("9\u{FE0F}\u{20E3}"),
        '+' => Some("\u{2795}"),
        '-' => Some("\u{2796}"),
        _ => None,
    }
}

fn main() {
    let input = env::args().skip(1).collect::<Vec<String>>().join(" ");

    let translated = {
        let mut translated = String::new();
        for (i, c) in input.chars().enumerate() {
            match translate(c) {
                Some(t) => translated.push_str(
                    &(if i == input.len() - 1 {
                        t.to_owned()
                    } else {
                        format!("{}\u{200B}", t)
                    }),
                ),
                None => translated.push(c),
            }
        }
        translated
    };
    println!("{}", translated);
}
