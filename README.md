# blkspk

Convert normal alphanumeric text to its emoji equivalent.

## Usage

```sh
blkspk "(your input here)"
```
